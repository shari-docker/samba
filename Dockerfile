FROM alpine:3.21
RUN apk --no-cache add samba supervisor bash
RUN mkdir /config /shared
COPY *.conf /config/
VOLUME /shared
EXPOSE 135/tcp 137/udp 138/udp 139/tcp 445/tcp
ENTRYPOINT ["supervisord", "-c", "/config/supervisord.conf"]
